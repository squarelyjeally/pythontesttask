coreapi==2.3.3
Django==2.2
djangorestframework==3.9.2
psycopg2==2.7.7
pytz==2019.1
requests==2.21.0
sqlparse==0.3.0
