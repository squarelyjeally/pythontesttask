from rest_framework .views import APIView
from rest_framework .viewsets import ReadOnlyModelViewSet
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED

from posts_back.serializers import PostSerializer, PostLikeUnlikeViewSerializer, PostCreateViewSerializer
from posts_back.models import Post
from posts.const import LIKE_ACTION, UNLIKE_ACTION


class ShowAllPostsViewSet(ReadOnlyModelViewSet):
    """
    API for posts viewing
    """
    # TODO: Add pagination
    
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class CreatePostView(APIView):
    """
    API for post creating
    post example:
    {
        "title": "sample_post"
        "post_body": "Some text not longer than 255 chars"
    }
    """
    serializer_class = PostCreateViewSerializer

    def post(self, request):

        input_serializer = self.serializer_class(data=request.data)
        input_serializer.is_valid(raise_exception=True)
        input_data = input_serializer.data
        input_data.update({"author": self.request.user.pk})
        serializer = PostSerializer(data=input_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(status=HTTP_201_CREATED, data=serializer.data)


class LikeUnlikePostView(APIView):
    """
    API for post like
    put example:
    {
        "post_id": 1
    }
    """
    serializer_class = PostLikeUnlikeViewSerializer

    def put(self, request):

        def update_post(post_id, action):
            posts_filter_result = Post.objects.filter(id=post_id)
            if posts_filter_result.exists():
                if action == LIKE_ACTION:
                    posts_filter_result.first().liked_by.add(request.user)
                elif action == UNLIKE_ACTION:
                    posts_filter_result.first().liked_by.remove(request.user)

        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        input_data = serializer.data
        update_post(input_data.get('post_id'), input_data.get('action'))

        return Response(status=HTTP_200_OK)
