## Posts backend

## Getting Started

documentation - {domain}/docs

admin - {domain}/admin

#### Prerequisites

You have to enable python virtual environment - <b>pyvenv <env_name></b>

Install all requirements - <b>pip install -r requirements.txt</b>

To launch test_bot type: <b>python manage.py test --no-input</b>
