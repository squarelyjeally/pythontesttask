from django.contrib.auth.models import User
from django.db import models


class Post(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    liked_by = models.ManyToManyField(User, related_name='likes', blank=True)
    title = models.CharField(max_length=255)
    post_body = models.TextField()

    def __str__(self):
        return '{}: {}'.format(self.author, self.title)
