from django.conf.urls import url
from django.urls import include
from rest_framework import routers

from posts_back.views import CreatePostView, LikeUnlikePostView, ShowAllPostsViewSet

router = routers.SimpleRouter()
router.register(r'posts', ShowAllPostsViewSet)


urlpatterns = [
    url(r'create_post/', CreatePostView.as_view(), name='api'),
    url(r'update_post/', LikeUnlikePostView.as_view(), name='api'),
    url(r'', include(router.urls), name='api'),
]
