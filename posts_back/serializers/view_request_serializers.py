from rest_framework import serializers
from posts.const import LIKE_ACTION, UNLIKE_ACTION


class PostLikeUnlikeViewSerializer(serializers.Serializer):
    post_id = serializers.IntegerField()
    action = serializers.ChoiceField(choices=[LIKE_ACTION, UNLIKE_ACTION])


class PostCreateViewSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=100)
    post_body = serializers.CharField(max_length=255)
