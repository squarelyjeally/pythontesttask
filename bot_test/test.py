from random import randint
from unittest.mock import patch

from django.test import TestCase
from rest_framework.test import APIClient

from bot_test.conf import MAX_POSTS_PER_USER_VALUE, MAX_USER_LIKES, MAX_USERS
from posts.const import LIKE_ACTION
from posts_back.models import Post


class PostsPipeTest(TestCase):

    def setUp(self):
        """Setting data for test."""

        self.users_count = randint(1, MAX_USERS)
        self.posts_per_user_count = randint(1, MAX_POSTS_PER_USER_VALUE)
        self.likes_per_user_count = randint(1, MAX_USER_LIKES)
        if self.likes_per_user_count > self.posts_per_user_count * self.users_count:
            self.likes_per_user_count = self.posts_per_user_count * self.users_count
        username_template = "username_%s"
        email_template = 'test_%s@qweas.com'
        password_template = 'password123'
        self.clients = list()
        self.created_users_list = list()

        for i in range(0, self.users_count):
            client = APIClient()
            request_data = \
                {'email': email_template % i,
                 'username': username_template % i,
                 'password': password_template}

            with patch('auth.serializers.model_serializers.UserSerializer.validate_email',
                       return_value=email_template % i) as call:
                client.post('/user/create/', data=request_data)

            request_data.pop('email')
            token = client.post('/user/login/', data=request_data).data['token']
            client.credentials(HTTP_AUTHORIZATION='Token ' + token)
            self.clients.append(client)

        self.maxDiff = None

    def test_posts_list(self):
        some_title_template = 'title_by_number_%s'
        some_post_body_template = 'Text body made by number_%s. Never try this at home. Some generated text by user.'

        # posts creating
        for client in self.clients:
            for post_number in range(0, self.posts_per_user_count):
                request_data = \
                    {'title': some_title_template % post_number,
                     'post_body': some_post_body_template % post_number}
                client.post('/api/create_post/', data=request_data)

        latest_id = Post.objects.last().id

        # posts like
        for client in self.clients:
            posts_id_to_like_list = list()

            for i in range(0, self.likes_per_user_count):
                posts_id_to_like_list.append(randint(1, latest_id))

            for post_id in posts_id_to_like_list:
                request_data = \
                    {'post_id': post_id,
                     'action': LIKE_ACTION}
                client.put('/api/update_post/', data=request_data)

        liked_by_values = Post.objects.filter(liked_by__isnull=False).values_list('liked_by', flat=True).count()
        all_posts_count = Post.objects.all().count()
        self.assertLessEqual(liked_by_values, self.likes_per_user_count * self.users_count)
        self.assertEqual(self.posts_per_user_count * self.users_count, all_posts_count)
