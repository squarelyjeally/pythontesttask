import json

import requests
from django.conf import settings
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.validators import UniqueValidator

from posts.const import VERIFY_EMAIL_LINK


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True, validators=[UniqueValidator(queryset=User.objects.all())])
    username = serializers.CharField(validators=[UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(min_length=4, write_only=True)

    def validate_email(self, email):
        resp_text = requests.get(VERIFY_EMAIL_LINK % (email, settings.EMAILHUNTER_API_KEY))
        resp_data = json.loads(resp_text.text)['data']
        if not resp_data['regexp']:
            raise ValidationError('Such email doesn\'t exists.')
        elif not resp_data['smtp_server']:
            raise ValidationError('Such email doesn\'t exists.')
        return email

    def create(self, validated_data):

        user = User.objects.create_user(
            username=validated_data['username'],
            email=validated_data['email'],
            password=validated_data['password'])
        return user

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')
