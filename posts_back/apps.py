from django.apps import AppConfig


class PostsBackConfig(AppConfig):
    name = 'posts_back'
